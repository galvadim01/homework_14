// 1

const person = {
    name: 'John',
    age: 30,
    gender: 'male'
}

let newPerson = {}

Object.assign(newPerson, person)

console.log(person)

newPerson.age = 35 

console.log(newPerson)

// 2 

const car = {
    make: 'Toyota',
    model: 'Corolla',
    year: 2015
  };
  
function printCarInfo(car) {
    if (car.year >  2001){
        console.log(car)
    } else {
        console.log('Машина занадто стара.')
    }
  }

  printCarInfo(car)

// 3
let = str = 'JavaScript is a high-level programming language'

function countWords(str) {
   return str.split(' ').length
}
    
console.log(countWords(str))

// 4

let string = 'JavaScript is awesome!';

function reverseString(str) {
 return string.split('').reverse().join('')
}

console.log(reverseString(string))

// 5


function reverseWordsInString(str_1) {

    const words = str_1.split(' ');

    const reversedWords = words.map(word => word.split('').reverse().join(''));

    const reversedString = reversedWords.join(' ');
    
    return reversedString;

}

const str_1 = 'JavaScript is awesome!';

console.log(reverseWordsInString(str_1)); 
